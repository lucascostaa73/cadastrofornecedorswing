package visao;

import controle.*;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.*;
import modelo.*;


/**
 *
 * @author pc vaio
 */
public class InterfaceCadastro extends javax.swing.JFrame {

    /**
     * Creates new form InterfaceCadastro
     */
    public InterfaceCadastro() {
        initComponents();
        limparTela();
        habilitarCampos(false);
    }

        ControleFornecedor umControleFornecedor = new ControleFornecedor();
        
        
        DefaultListModel ListModel1 = new DefaultListModel();
        DefaultListModel ListModel2 = new DefaultListModel();
        
        //private ArrayList<Produto> listaProdTeste;
        ArrayList<Produto> listaProdutos = new ArrayList<Produto>();
        
        String stringAux1, stringAux2;
        int intAux;
        boolean modoAlterar = false;
       
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jSpinner1 = new javax.swing.JSpinner();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListFornecedores = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();
        jButtonRemoverFornecedor = new javax.swing.JButton();
        jButtonNovo = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldRua = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldNumero = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldBairro = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextFieldCidade = new javax.swing.JTextField();
        jTextFieldEstado = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListProdutos = new javax.swing.JList();
        jButtonAdicionarProduto = new javax.swing.JButton();
        jButtonRemoverProduto = new javax.swing.JButton();
        jTextFieldProduto = new javax.swing.JTextField();
        jButtonSalvar = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jTextFieldValorProduto = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jTextFieldQuantidadeProduto = new javax.swing.JTextField();
        jTextFieldCpf = new javax.swing.JTextField();
        jTextFieldCnpj = new javax.swing.JTextField();
        jComboBoxPessoaFJ = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jTextFieldRazaoSocial = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldTelefone = new javax.swing.JTextField();
        jTextFieldNome = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();

        jTextField1.setText("jTextField1");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 234, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 136, Short.MAX_VALUE)
        );

        jListFornecedores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListFornecedoresMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jListFornecedores);

        jLabel1.setText("Fornecedores");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jLabel1)
                        .addGap(70, 70, 70))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(289, 289, 289)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(187, Short.MAX_VALUE))
        );

        jButtonRemoverFornecedor.setText("Remover");
        jButtonRemoverFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoverFornecedorActionPerformed(evt);
            }
        });

        jButtonNovo.setText("Novo");
        jButtonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovoActionPerformed(evt);
            }
        });

        jLabel2.setText("Cadastro de Fornecedor");

        jLabel5.setText("Endereço");

        jTextFieldRua.setText("jTextField2");

        jLabel6.setText("Rua");

        jTextFieldNumero.setText("jTextField3");

        jLabel7.setText("Numero");

        jLabel8.setText("Bairro");

        jTextFieldBairro.setText("jTextField6");

        jLabel9.setText("Cidade");

        jTextFieldCidade.setText("jTextField7");

        jTextFieldEstado.setText("jTextField8");

        jLabel10.setText("Estado");

        jLabel14.setText("Nome do Produto");

        jListProdutos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListProdutosMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jListProdutos);

        jButtonAdicionarProduto.setText("Adicionar Produto");
        jButtonAdicionarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarProdutoActionPerformed(evt);
            }
        });

        jButtonRemoverProduto.setText("Remover");
        jButtonRemoverProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoverProdutoActionPerformed(evt);
            }
        });

        jTextFieldProduto.setText("jTextField4");

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jLabel15.setText("Valor do Produto");

        jTextFieldValorProduto.setText("jTextField2");
        jTextFieldValorProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldValorProdutoActionPerformed(evt);
            }
        });

        jLabel16.setText("Quantidade");

        jTextFieldQuantidadeProduto.setText("jTextField2");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButtonSalvar))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jButtonAdicionarProduto, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButtonRemoverProduto, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextFieldProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldValorProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldQuantidadeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jTextFieldProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(jTextFieldValorProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(jTextFieldQuantidadeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonSalvar))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jButtonAdicionarProduto)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonRemoverProduto))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(21, Short.MAX_VALUE))))
        );

        jTextFieldCpf.setText("jTextField5");

        jTextFieldCnpj.setText("jTextField11");

        jComboBoxPessoaFJ.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pessoa jurídica", "Pessoa Física" }));
        jComboBoxPessoaFJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxPessoaFJActionPerformed(evt);
            }
        });

        jLabel12.setText("Cnpj");

        jLabel13.setText("Razão Social");

        jTextFieldRazaoSocial.setText("jTextField12");

        jLabel11.setText("Cpf");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel6))
                                .addGap(14, 14, 14)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel5)
                                    .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldBairro, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
                                    .addComponent(jTextFieldRua)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel11)
                                .addComponent(jLabel12)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBoxPessoaFJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTextFieldCpf)
                                .addComponent(jTextFieldCnpj)
                                .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(28, 28, 28))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jTextFieldRua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jComboBoxPessoaFJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(jTextFieldCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel3.setText("Nome");

        jTextFieldTelefone.setText("jTextField10");

        jTextFieldNome.setText("jTextField9");

        jLabel4.setText("Telefone");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jButtonNovo)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jButtonRemoverFornecedor))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(jLabel3)
                                            .addGap(23, 23, 23))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(jLabel4)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jTextFieldNome)
                                        .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE))))
                            .addGap(355, 355, 355))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(198, 198, 198)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jButtonNovo)
                                    .addComponent(jButtonRemoverFornecedor))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel3)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel4)))))
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoActionPerformed
        // TODO add your handling code here:
        habilitarCampos(true);
        jButtonSalvar.setEnabled(true);
        limparTela();
        ListModel2.clear();
    }//GEN-LAST:event_jButtonNovoActionPerformed

    private void jComboBoxPessoaFJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxPessoaFJActionPerformed
        // TODO add your handling code here:
        if(jButtonSalvar.isEnabled() == true){
           if (jComboBoxPessoaFJ.getSelectedIndex() == 0)
            {
                habilitarPessoaJuridica(true);
                habilitarPessoaFisica(false);
            }
            else
            {
                habilitarPessoaFisica(true);
                habilitarPessoaJuridica(false);
            } 
        }
        
    }//GEN-LAST:event_jComboBoxPessoaFJActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        // TODO add your handling code here:      
        if(modoAlterar == false)
        {
            String nome = jTextFieldNome.getText();
        
            String telefone = jTextFieldTelefone.getText();

            String rua = jTextFieldRua.getText();
            String cidade = jTextFieldCidade.getText();
            String estado = jTextFieldEstado.getText();
            String bairro = jTextFieldBairro.getText();
            String numero = jTextFieldNumero.getText();
         
            String produto =  jTextFieldProduto.getText();
            String cpf =  jTextFieldCpf.getText();
            String cnpj =  jTextFieldCnpj.getText();
            String razaoSocial = jTextFieldRazaoSocial.getText();
               
            Endereco umEndereco = new Endereco();
        
            umEndereco.setRua(rua);
            umEndereco.setBairro(bairro);
            umEndereco.setCidade(cidade);
            umEndereco.setEstado(estado);
            umEndereco.setNumero(numero);
        
        
             if(jComboBoxPessoaFJ.getSelectedIndex() == 0)
              {
                     PessoaJuridica umFornecedor = new PessoaJuridica(nome, telefone);
                     umFornecedor.setEndereco(umEndereco);
                     umFornecedor.adicionarTodosProdutos(listaProdutos);
            
                    umControleFornecedor.adicionar(umFornecedor);
                }
                if(jComboBoxPessoaFJ.getSelectedIndex() == 1)
                {
                    PessoaFisica umFornecedor = new PessoaFisica(nome, telefone);
                    umFornecedor.setEndereco(umEndereco);
                    umFornecedor.adicionarTodosProdutos(listaProdutos);
            
                    umControleFornecedor.adicionar(umFornecedor);
                }

            ListModel1.addElement(nome);
        
            jListFornecedores.setModel(ListModel1);
        
        
            listaProdutos.removeAll(listaProdutos);
            ListModel2.clear();
        
            habilitarCampos(false);
            limparTela();
        }
        
        else
        {
                        String nome = jTextFieldNome.getText();
        
            String telefone = jTextFieldTelefone.getText();

            String rua = jTextFieldRua.getText();
            String cidade = jTextFieldCidade.getText();
            String estado = jTextFieldEstado.getText();
            String bairro = jTextFieldBairro.getText();
            String numero = jTextFieldNumero.getText();
         
            String produto =  jTextFieldProduto.getText();
            String cpf =  jTextFieldCpf.getText();
            String cnpj =  jTextFieldCnpj.getText();
            String razaoSocial = jTextFieldRazaoSocial.getText();
               
            Endereco umEndereco = new Endereco();
        
            umEndereco.setRua(rua);
            umEndereco.setBairro(bairro);
            umEndereco.setCidade(cidade);
            umEndereco.setEstado(estado);
            umEndereco.setNumero(numero);
        
            ListModel1.removeElement(stringAux1);
            
             if(jComboBoxPessoaFJ.getSelectedIndex() == 0)
              {
                    Fornecedor umFornecedor = umControleFornecedor.pesquisar(stringAux1);
                    umFornecedor.setNome(nome);
                    umFornecedor.setTelefone(telefone);
                    umFornecedor.setEndereco(umEndereco);
                    umFornecedor.adicionarTodosProdutos(listaProdutos);
            
                }
                if(jComboBoxPessoaFJ.getSelectedIndex() == 1)
                {
                    Fornecedor umFornecedor = umControleFornecedor.pesquisar(nome);
                    umFornecedor.setNome(nome);
                    umFornecedor.setTelefone(telefone);
                    umFornecedor.setEndereco(umEndereco);
                    umFornecedor.adicionarTodosProdutos(listaProdutos);
                }

            //ListModel1.set(jListFornecedores.getSelectedIndex(),nome);
            
            ListModel1.addElement(nome);
            jListFornecedores.setModel(ListModel1);
        
        
            listaProdutos.removeAll(listaProdutos);
            ListModel2.clear();
        
            habilitarCampos(false);
            limparTela();
        }
        modoAlterar = false;
        
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jTextFieldValorProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldValorProdutoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldValorProdutoActionPerformed

    private void jButtonAdicionarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarProdutoActionPerformed
        // TODO add your handling code here:
        
        String nomeProduto = jTextFieldProduto.getText();
        String valorProduto = jTextFieldValorProduto.getText();
        String quantidadeProduto = jTextFieldQuantidadeProduto.getText();
        
        Produto umProduto = new Produto(nomeProduto, valorProduto, quantidadeProduto);

        listaProdutos.add(umProduto);
        
        ListModel2.addElement(nomeProduto);
         
        jListProdutos.setModel(ListModel2);
        jTextFieldValorProduto.setText("");
        jTextFieldQuantidadeProduto.setText("");
        jTextFieldProduto.setText("");
        
        //Implementar lista de produtos dos fornecedores
    }//GEN-LAST:event_jButtonAdicionarProdutoActionPerformed

    private void jListFornecedoresMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListFornecedoresMouseClicked
        // TODO add your handling code here:
        ListModel2.removeAllElements();
        ListModel2.clear();
        String nomeSelecionado = (String) jListFornecedores.getSelectedValue();
        stringAux1 = nomeSelecionado;
        
        /*
        if(umControleFornecedor.pesquisar(nomeSelecionado).getClass().equalsIgnoreCase( "PessoaJuridica"))
        {
            
        }
        */
        
        //PessoaJuridica umFornecedor = (PessoaJuridica) umControleFornecedor.pesquisar(nomeSelecionado); -> Problema: aparacer Nome
        
        Fornecedor umFornecedor =  umControleFornecedor.pesquisar(nomeSelecionado);
        
        jTextFieldNome.setText(umFornecedor.getNome());
        jTextFieldTelefone.setText(umFornecedor.getTelefone());
        
        //jTextFieldNome.setText(umFornecedor.getCpf());
        //jTextFieldNome.setText(umFornecedor.getCnpj());
        //jTextFieldNome.setText(umFornecedor.getRazaoSocial());
        
        jTextFieldRua.setText(umFornecedor.getEndereco().getRua());
        jTextFieldCidade.setText(umFornecedor.getEndereco().getCidade());
        jTextFieldEstado.setText(umFornecedor.getEndereco().getEstado());
        jTextFieldBairro.setText(umFornecedor.getEndereco().getBairro());
        jTextFieldNumero.setText(umFornecedor.getEndereco().getNumero());
        
        //Como Comparar duas Classes umFornecedor.getClass() == PessoaJuridica -> False;
        
        
        for(int count = 0; count < umFornecedor.getSizeLista(); count++)
        {
            ListModel2.addElement(umFornecedor.pesquisarProduto(count).getNome());           
        }

        jListProdutos.setModel(ListModel2);
        
        jButtonRemoverFornecedor.setEnabled(true);
        
        modoAlterar = true;
        habilitarCampos(true);
        
        
    }//GEN-LAST:event_jListFornecedoresMouseClicked

    private void jButtonRemoverFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverFornecedorActionPerformed
        // TODO add your handling code here:
        Fornecedor umFornecedor = umControleFornecedor.pesquisar(stringAux1);
        
        umControleFornecedor.remover(umFornecedor);
        
        ListModel1.removeElement(stringAux1);
        jListFornecedores.setModel(ListModel1);
        
        habilitarCampos(false);
        limparTela();
        jButtonRemoverFornecedor.setEnabled(false);
    }//GEN-LAST:event_jButtonRemoverFornecedorActionPerformed

    private void jListProdutosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListProdutosMouseClicked
        // TODO add your handling code here:
        if(modoAlterar == false)
        {
            String nomeSelecionado = (String) jListProdutos.getSelectedValue();
            intAux = jListProdutos.getSelectedIndex();
            Produto umProduto = listaProdutos.get(intAux);
        
           //jTextFieldValorProduto.setText(umProduto.getValor());
          // jTextFieldQuantidadeProduto.setText(umProduto.getQuantidade());
          // jTextFieldProduto.setText(umProduto.getNome());
        
            jButtonRemoverProduto.setEnabled(true);
        }
        if(modoAlterar == true)
        {
            String nomeSelecionado = (String) jListProdutos.getSelectedValue();
            intAux = jListProdutos.getSelectedIndex();
            Produto umProduto = umControleFornecedor.pesquisar(stringAux1).pesquisarProduto(intAux);
        
            jButtonRemoverProduto.setEnabled(true);
        }
    }//GEN-LAST:event_jListProdutosMouseClicked

    private void jButtonRemoverProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverProdutoActionPerformed
        // TODO add your handling code here:
        if(modoAlterar == false)
        {
            Produto umProduto = listaProdutos.get(intAux);
            
            listaProdutos.remove(umProduto);
            ListModel2.removeElement(umProduto.getNome());
        }
        if(modoAlterar == true)
        {
            Fornecedor umFornecedor = umControleFornecedor.pesquisar(stringAux1);
            Produto umProduto = umFornecedor.pesquisarProduto(intAux);
            
            umFornecedor.removerProduto(umProduto);
            ListModel2.removeElement(umProduto.getNome());

        }
        
    }//GEN-LAST:event_jButtonRemoverProdutoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfaceCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfaceCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfaceCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfaceCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InterfaceCadastro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdicionarProduto;
    private javax.swing.JButton jButtonNovo;
    private javax.swing.JButton jButtonRemoverFornecedor;
    private javax.swing.JButton jButtonRemoverProduto;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JComboBox jComboBoxPessoaFJ;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JList jListFornecedores;
    private javax.swing.JList jListProdutos;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextFieldBairro;
    private javax.swing.JTextField jTextFieldCidade;
    private javax.swing.JTextField jTextFieldCnpj;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldEstado;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldNumero;
    private javax.swing.JTextField jTextFieldProduto;
    private javax.swing.JTextField jTextFieldQuantidadeProduto;
    private javax.swing.JTextField jTextFieldRazaoSocial;
    private javax.swing.JTextField jTextFieldRua;
    private javax.swing.JTextField jTextFieldTelefone;
    private javax.swing.JTextField jTextFieldValorProduto;
    // End of variables declaration//GEN-END:variables
    
    private void limparTela() {
        jTextFieldNome.setText("");
        jTextFieldTelefone.setText("");
        
        jTextFieldNome.setText("");
        jTextFieldTelefone.setText("");
        
        jTextFieldRua.setText("");
        jTextFieldCidade.setText("");
        jTextFieldEstado.setText("");
        jTextFieldBairro.setText("");
        jTextFieldNumero.setText("");
        
        jTextFieldProduto.setText("");
        jTextFieldCpf.setText("");
        jTextFieldCnpj.setText("");
        jTextFieldRazaoSocial.setText("");
        
        jTextFieldValorProduto.setText("");
        jTextFieldQuantidadeProduto.setText("");
        
        jComboBoxPessoaFJ.setSelectedIndex(0);
        ListModel2.clear();
       
        jButtonRemoverFornecedor.setEnabled(false);
        jButtonRemoverProduto.setEnabled(false);
    }
    
    private void habilitarCampos(boolean habilitar) {
        
        jTextFieldNome.setEnabled(habilitar);
        jTextFieldTelefone.setEnabled(habilitar);
        
        jTextFieldRua.setEnabled(habilitar);
        jTextFieldCidade.setEnabled(habilitar);
        jTextFieldEstado.setEnabled(habilitar);
        jTextFieldBairro.setEnabled(habilitar);
        jTextFieldNumero.setEnabled(habilitar);
        
        jTextFieldProduto.setEnabled(habilitar);
        
        jTextFieldValorProduto.setEnabled(habilitar);
        jTextFieldQuantidadeProduto.setEnabled(habilitar);
        
        jTextFieldCpf.setEnabled(habilitar);
        jTextFieldCnpj.setEnabled(habilitar);
        jTextFieldRazaoSocial.setEnabled(habilitar);
        jComboBoxPessoaFJ.setEnabled(habilitar);
        
        jButtonAdicionarProduto.setEnabled(habilitar);
        jButtonRemoverProduto.setEnabled(habilitar);
        
        
        jButtonSalvar.setEnabled(habilitar);
        
        
        
    }
    
    private void habilitarPessoaFisica(boolean habilitar)
    {
        jTextFieldCpf.setEnabled(habilitar);
        jTextFieldCnpj.setText("");
        jTextFieldRazaoSocial.setText("");
        
    }
    private void habilitarPessoaJuridica(boolean habilitar)
    {
            jTextFieldCnpj.setEnabled(habilitar);
            jTextFieldRazaoSocial.setEnabled(habilitar);
            jTextFieldCpf.setText("");
    }

}