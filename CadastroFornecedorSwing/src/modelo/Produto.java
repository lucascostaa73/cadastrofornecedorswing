package modelo;

public class Produto {
	private String nome;
	private String Quantidade;
	private String Valor;
        
            
    public Produto(String nome, String Quantidade, String Valor)
    {
        this.nome = nome;
        this.Quantidade = Quantidade;
        this.Valor = Valor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getQuantidade() {
        return Quantidade;
    }

    public void setQuantidade(String Quantidade) {
        this.Quantidade = Quantidade;
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String Valor) {
        this.Valor = Valor;
    }	
}
