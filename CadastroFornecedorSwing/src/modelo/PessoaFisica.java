package modelo;

public class PessoaFisica extends Fornecedor{

	private String cpf;
	
	public PessoaFisica(String nome, String telefone,int posicaoArray, String cpf){
		super(nome, telefone);//Chamar a super classe
		this.cpf = cpf;// Com uma caracter�stica pr�pria 
	}
	
	public PessoaFisica(String nome, String telefone){
		super(nome, telefone);//Chamar a super classe
	}
	
	public void setCpf(String cpf){
		this.cpf = cpf;
	}
	public String getCpf() {
		return cpf;
	}
	
	
}
