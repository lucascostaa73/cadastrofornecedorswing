package modelo;

import java.util.ArrayList;

public class Fornecedor {
	
	private int posicaoArray;
	private String nome;
	private String telefone;
        private Endereco endereco;
        private ArrayList<Produto> listaProdutos;
	
	public Fornecedor(String nome, String telefone){
		this.nome = nome;
		this.telefone = telefone;
                listaProdutos = new ArrayList<Produto>();
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}               
        
        public Endereco getEndereco() {
            return endereco;
        }

        public void setEndereco(Endereco endereco) {
            this.endereco = endereco;
        }      
    //=============================================================    
        public void adicionarProduto(Produto umProduto)
        {
            listaProdutos.add(umProduto);
        }
        public void removerProduto(Produto umProduto)
        {
            listaProdutos.remove(umProduto);
        }
        
        public Produto pesquisarProduto(int index)
        {
		return listaProdutos.get(index);
	}
  
        public int getSizeLista()
        {            
            return listaProdutos.size();
        }
        
        public void adicionarTodosProdutos(ArrayList<Produto> umaListaProduto)
        {
            listaProdutos.addAll(umaListaProduto);
        }
        public Produto pesquisarProduto(String umNome){
		for(Produto umProduto : listaProdutos){
			if(umProduto.getNome().equalsIgnoreCase(umNome)) return umProduto;
		}
	return null;
	}

}
