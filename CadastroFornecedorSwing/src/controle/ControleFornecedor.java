package controle;
import java.util.ArrayList;
import modelo.Fornecedor;


public class ControleFornecedor {
	
	private ArrayList<Fornecedor> listaFornecedores;
	
	public ControleFornecedor(){
		listaFornecedores = new ArrayList<Fornecedor>();
	}
	
	
	public void adicionar(Fornecedor umFornecedor){
		listaFornecedores.add(umFornecedor);
	}
	public void remover(Fornecedor umFornecedor){
		listaFornecedores.remove(umFornecedor);
	}
	public Fornecedor pesquisar(String umNome){
		for(Fornecedor umFornecedor : listaFornecedores){
			if(umFornecedor.getNome().equalsIgnoreCase(umNome)) return umFornecedor;
		}
	return null;
	}
	
	public String pesquisar(int index){
		return listaFornecedores.get(index).getNome();
	}
}
