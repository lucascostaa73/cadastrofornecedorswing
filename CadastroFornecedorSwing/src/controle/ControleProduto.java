package controle;
import java.util.ArrayList;
import modelo.Produto;


public class ControleProduto {
	
	private ArrayList<Produto> listaProduto;
	
	public ControleProduto(){
		listaProduto = new ArrayList<Produto>();
	}

	public void remover(Produto umProduto){
		listaProduto.remove(umProduto);
	}
	public Produto pesquisar(String umNome){
		for(Produto umProduto : listaProduto){
			if(umProduto.getNome().equalsIgnoreCase(umNome)) return umProduto;
		}
	return null;
	}
	
	public Produto pesquisar(int index){
		return listaProduto.get(index);
	}

        public void adicionar(Produto umProduto) {
                listaProduto.add(umProduto); 
        }
        
        public int getSize()
        {            
            return listaProduto.size();
        }
        
        public void apagarLista()
        {
            listaProduto.removeAll(listaProduto);
        }
        
        public void adicionarTodos(ArrayList<Produto> umaListaProduto)
        {
            listaProduto.addAll(umaListaProduto);
        }

}